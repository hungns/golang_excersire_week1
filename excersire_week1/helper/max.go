package helper

import (
	"reflect"
)

func Max(slice interface{}) interface{} {
	sliceVal := reflect.ValueOf(slice)

	if sliceVal.Kind() == reflect.Slice || sliceVal.Kind() == reflect.Array {
		flag := sliceVal.Index(0)
		for i := 0; i < sliceVal.Len(); i++ {
			val := sliceVal.Index(i)
			if reflect.ValueOf(flag.Interface()).Kind() == reflect.Int {
				if flag.Int() < val.Int() {
					flag = val
				}
			} else if reflect.ValueOf(flag.Interface()).Kind() == reflect.Float32 || reflect.ValueOf(flag.Interface()).Kind() == reflect.Float64 {
				if flag.Float() < val.Float() {
					flag = val
				}
			} else if reflect.ValueOf(flag.Interface()).Kind() == reflect.String {
				if flag.String() < val.String() {
					flag = val
				}
			}
		}
		return flag.Interface()
	}
	return nil
}
