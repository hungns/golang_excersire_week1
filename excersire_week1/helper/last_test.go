package helper

import (
	"reflect"
	"testing"
)

func TestLast(t *testing.T) {
	type args struct {
		arr interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{

			name: "Test with empty array or slice",
			args: args{
				arr: []string{},
			},
			want: nil,
		},
		{

			name: "Test with  array or slice int",
			args: args{
				arr: []int{1, 6, 9, 2},
			},
			want: 2,
		},
		{

			name: "Test with  array or slice string",
			args: args{
				arr: []string{"hung", "cuong", "acb"},
			},
			want: "acb",
		},
		{

			name: "Test with  array 1 value",
			args: args{
				arr: []int{1},
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Last(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Last() = %v, want %v", got, tt.want)
			}
		})
	}
}
