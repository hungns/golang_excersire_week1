package helper

import (
	"testing"
)

func TestContainsInt(t *testing.T) {
	arrInt := []int{1, 5, 2, 3, 4}
	key := 5
	expect := true
	actual := Contains(arrInt, key)
	if actual != expect {
		t.Errorf("Error  slice int: %v, but actual: %v", expect, actual)
	}

	key2 := 8
	expect2 := false
	actual2 := Contains(arrInt, key2)
	if actual2 != expect2 {
		t.Errorf("Error  slice int: %v, but actual: %v", expect2, actual2)
	}
}

func TestContainsString(t *testing.T) {
	arrInt := []string{"aaa", "bb", "aa", "bbc", "c"}
	key := "bb"
	expect := true
	actual := Contains(arrInt, key)
	if actual != expect {
		t.Errorf("Error  slice int: %v, but actual: %v", expect, actual)
	}

	key2 := "cbb"
	expect2 := false
	actual2 := Contains(arrInt, key2)
	if actual2 != expect2 {
		t.Errorf("Error  slice int: %v, but actual: %v", expect2, actual2)
	}
}

func TestContainsFloat(t *testing.T) {
	arrInt := []float32{1.0, 4.0, 7.5, 2.1, 10.8}
	var key float32 = 7.5
	expect := true
	actual := Contains(arrInt, key)
	if actual != expect {
		t.Errorf("Error  slice int: %v, but actual: %v", expect, actual)
	}

	var key2 float32 = 10.75
	expect2 := false
	actual2 := Contains(arrInt, key2)
	if actual2 != expect2 {
		t.Errorf("Error  slice int: %v, but actual: %v", expect2, actual2)
	}
}
