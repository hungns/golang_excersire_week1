package helper

import (
	"testing"
)

func TestMaxInt(t *testing.T) {

	arrInt := []int{1, 5, 2, 3, 4}
	expect := 5
	actual := Max(arrInt)
	if actual != expect {
		t.Errorf("Error max slice int: %v, but actual: %v", expect, actual)
	}

	arrInt1 := [5]int{1, 5, 2, 3, 4}
	expect1 := 5
	actual1 := Max(arrInt1)
	if actual1 != expect1 {
		t.Errorf("Error max array int: %v, but actual: %v", expect1, actual1)
	}
}

func TestMaxFloat(t *testing.T) {
	arrFloat := []float32{1.0, 55.8, 22.3, 31.1, 40.4}
	var expect float32 = 55.8
	actual := Max(arrFloat)
	if actual != expect {
		t.Errorf("Error max slice float: %v, but actual: %v", expect, actual)
	}

	arrFloat1 := [5]float32{1.0, 55.8, 22.3, 31.1, 40.4}
	var expect1 float32 = 55.8
	actual1 := Max(arrFloat1)
	if actual1 != expect1 {
		t.Errorf("Error max array float: %v, but actual: %v", expect1, actual1)
	}
}

func TestMaxString(t *testing.T) {
	arrString := []string{"aa", "a1", "ac", "de", "d1"}
	expect := "de"
	actual := Max(arrString)
	if actual != expect {
		t.Errorf("Error max slice string: %v, but actual: %v", expect, actual)
	}

	arrString1 := [5]string{"aa", "a1", "ac", "de", "d1"}
	expect1 := "de"
	actual1 := Max(arrString1)
	if actual1 != expect1 {
		t.Errorf("Error max array string: %v, but actual: %v", expect1, actual1)
	}
}
