package helper

import (
	"testing"
)

func TestIsEmptyString(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			// TODO: Add test cases.
			name: "Test with empty string",
			args: args{
				val: "",
			},
			want: true,
		},
		{
			// TODO: Add test cases.
			name: "Test with empty string",
			args: args{
				val: "abc",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsEmptyString(tt.args.val); got != tt.want {
				t.Errorf("IsEmtryString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsEmpty(t *testing.T) {
	type args struct {
		val interface{}
	}
	type testcase struct {
		name string
		args args
		want bool
	}
	tests := []testcase{
		testcase{
			// TODO: Add test cases.
			name: "Test with empty string",
			args: args{
				val: "",
			},
			want: true,
		},
		testcase{
			// TODO: Add test cases.
			name: "Test with empty string",
			args: args{
				val: "abc",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsEmpty(tt.args.val); got != tt.want {
				t.Errorf("IsEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsEmptySlice(t *testing.T) {
	input := []int{}
	expect := true
	actual := IsEmpty(input)
	if actual != expect {
		t.Errorf("Error: %v, but actual: %v", expect, actual)
	}

}

func TestIsEmptyArray(t *testing.T) {
	input := []int{}
	expect := true
	actual := IsEmpty(input)
	if actual != expect {
		t.Errorf("Error: %v, but actual: %v", expect, actual)
	}

}

func TestIsEmptyStruct(t *testing.T) {
	type user struct {
		Name string
		Pass string
	}
	input := user{}
	expect := true
	actual := IsEmpty(input)
	if actual != expect {
		t.Errorf("Error: %v, but actual: %v", expect, actual)
	}

	input2 := user{}
	input2.Name = "Group 4"
	expect2 := false
	actual2 := IsEmpty(input2)
	if actual2 != expect2 {
		t.Errorf("Error: %v, but actual: %v", actual2, expect2)
	}
}
