package helper

import (
	"reflect"
)

func Map(input interface{}, fn interface{}) interface{} {
	inputVal := reflect.ValueOf(input)
	if inputVal.Kind() != reflect.Slice && inputVal.Kind() != reflect.Array {
		panic("Input invalid")
	}
	inputFn := reflect.ValueOf(fn)
	if inputFn.Kind() != reflect.Func {
		panic("Input invalid")
	}
	outputFnValue := inputFn.Type().Out(0)
	typeOfResult := reflect.SliceOf(outputFnValue)
	result := reflect.MakeSlice(typeOfResult, 0, 0)
	for i := 0; i < inputVal.Len(); i++ {
		ele := inputVal.Index(i)
		out := inputFn.Call([]reflect.Value{ele})[0]
		result = reflect.Append(result, out)
	}
	return result.Interface()
}
