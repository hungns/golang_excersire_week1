package helper

import (
	"reflect"
	"testing"
)

func TestMap(t *testing.T) {
	input := []int{4, 8}
	expect := []int{16, 64}
	actual := Map(input, func(n int) int {
		return n * n
	})
	if reflect.DeepEqual(expect, actual) == false {
		t.Errorf("Error: %v, but actual: %v", expect, actual)
	}

	input1 := []float32{5.0, 10.0}
	expect1 := []float32{20.0, 40.0}
	actual1 := Map(input1, func (n float32) float32 {
		return n * 4
	})
	if reflect.DeepEqual(expect1, actual1) == false {
		t.Errorf("Error: %v, but actual: %v", expect1, actual1)
	}
}
