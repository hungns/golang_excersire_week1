package helper

import (
	"reflect"
)

func Contains(slice interface{}, key interface{}) bool {
	sliceVal := reflect.ValueOf(slice)
	keyVal := reflect.ValueOf(key)
	if sliceVal.Kind() != reflect.Slice && sliceVal.Kind() != reflect.Array {
		panic("Input invalid")
	}
	res := false
	for i := 0; i < sliceVal.Len(); i++ {
		val := sliceVal.Index(i)
		if reflect.DeepEqual(keyVal.Interface(), val.Interface()) {
			res = true
			break
		}
	}
	return res
}
