package helper

import "reflect"

func IsEmpty(input interface{}) bool {
	if input == "" || input == 0 {
		return true
	}
	inputVal := reflect.ValueOf(input)
	if inputVal.Kind() == reflect.Slice || inputVal.Kind() == reflect.Array {
		if inputVal.Len() == 0 {
			return true
		}
	}
	if inputVal.Kind() == reflect.Struct {
		for i := 0; i < inputVal.NumField(); i++ {
			field := inputVal.Field(i)
			val := field.Interface()
			if val != "" && val != 0 {
				return false
			}
		}
		return true
	}
	return false
}

/*
IsEmptyString
*/
func IsEmptyString(val string) bool {
	//This is empty
	if val == "" {
		return true
	}
	return false
}
