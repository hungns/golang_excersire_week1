package helper

import (
	"reflect"
)

func Last(arr interface{}) interface{} {
	val := reflect.ValueOf(arr)
	if val.Kind() != reflect.Slice && val.Kind() != reflect.Array {
		panic("Input invalid")
	}
	if val.Len() == 0 {
		return nil
	}
	return val.Index(val.Len() - 1).Interface()
}
